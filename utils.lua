-- function to zero out tensors
function zeroDataSize(data)
  if type(data) == 'table' then
    for i = 1, #data do
      data[i] = zeroDataSize(data[i])
    end
  elseif type(data) == 'userdata' then
    data = torch.Tensor():typeAs(data)
  end
  return data
end

-- Resize the output, gradInput, etc temporary tensors to zero (so that the on disk size is smaller)
function cleanupModel(node)
  local cache = {}
  if node.output ~= nil then
    cache.output = node.output
    node.output = zeroDataSize(node.output)
  end
  if node.gradInput ~= nil then
    cache.gradInput = node.gradInput
    node.gradInput = zeroDataSize(node.gradInput)
  end
  if node.finput ~= nil then
    cache.finput = node.finput
    node.finput = zeroDataSize(node.finput)
  end
  -- Recurse on nodes with 'modules'
  if (node.modules ~= nil) then
    if (type(node.modules) == 'table') then
      cache.modules = {}
      for i = 1, #node.modules do
        local child = node.modules[i]
        cache.modules[i] = cleanupModel(child)
      end
    end
  end
  collectgarbage()
  return cache
end

function restoreModel(node, cache)
  if node.output ~= nil then
    node.output = cache.output
  end
  if node.gradInput ~= nil then
    node.gradInput = cache.gradInput
  end
  if node.finput ~= nil then
    node.finput = cache.finput
  end
  -- Recurse on nodes with 'modules'
  if (node.modules ~= nil) then
    if (type(node.modules) == 'table') then
      for i = 1, #node.modules do
        restoreModel(node.modules[i], cache.modules[i])
      end
    end
  end
  collectgarbage()
end
