
local transition = {}

function transition:new(opt)
  opt = opt or {}
  assert(opt.state_size, 'State size must be provided!')
  -- set parameters
  local table = {
    state_size = opt.state_size,
    extra_state_size = opt.extra_state_size or 0,
    size = opt.size or 1000000,
    -- probability of discarding a non-reward sequence when sampling
    non_reward_prob = opt.non_reward_prob or 0,
    numEntries = 0,
    insertIndex = 0,
    currentEpisodeStart = -1, -- negative indicates that there is no current episode
    episodes = {},
    n_samples = 0,
    n_discarded_samples = 0,
  }

  table.s = torch.ByteTensor(table.size, table.state_size + table.extra_state_size):fill(0)
  table.a = torch.ByteTensor(table.size):fill(0) -- only have support for 255 actions
  table.r = torch.FloatTensor(table.size):fill(0)
  table.t = torch.ByteTensor(table.size):fill(0)

  setmetatable(table, self)
  self.__index = self
  return table
end

function transition:reset()
  self.numEntries = 0
  self.insertIndex = 0
  self.episodes = {}
  self.currentEpisodeStart = -1
  self.n_samples = 0
  self.n_discarded_samples = 0
end

function transition:reset_episode()
  self.currentEpisodeStart = -1
end

function transition:count()
  return self.numEntries
end

function transition:statistics()
  -- calculate average length of episodes
  local lens = torch.FloatTensor(#self.episodes)
  for i = 1, #self.episodes do
    local len = (self.episodes[i].finish - self.episodes[i].start) + 1
    if len < 0 then len = self.size + len end -- remember that len is negative
    lens[i] = len
  end
  -- cache statistics
  self.episode_median, _ = lens:median()
  self.episode_median = self.episode_median[1]
  self.episode_mean = lens:mean()
  self.episode_std = lens:std()
  self.episode_min = lens:min()
  self.episode_max = lens:max()
  -- sampling statistics (from last time statistics() was called)
  local n_samples = self.n_samples or 0
  local n_discarded_samples = self.n_discarded_samples or 0
  self.n_samples = 0
  self.n_discarded_samples = 0
  return #self.episodes, self.episode_median, self.episode_mean, self.episode_std, self.episode_min, self.episode_max, n_samples, n_discarded_samples
end

-- adds the information for a single timestep
-- s: state at time 't'
-- t: if the state at time 't' (s) is terminal
-- a: the action selected at time 't'
-- r: reward at time 't' (on transition TO 's' after taking action at time 't-1')
function transition:add(s, t, a, r)
  assert(s, 'State cannot be nil')
  assert(a, 'Action cannot be nil')
  assert(r, 'Reward cannot be nil')
  assert(t == true or t == false, 'Terminal must be boolean')

  -- handle size and insert index
  if self.numEntries < self.size then
    self.numEntries = self.numEntries + 1
  end

  self.insertIndex = self.insertIndex + 1
  if self.insertIndex > self.size then
    self.insertIndex = 1
  end

  -- Insert (s, a, r, t)
  self.s[self.insertIndex]:copy(s)
  self.a[self.insertIndex] = a
  self.r[self.insertIndex] = r
  if t then
    self.t[self.insertIndex] = 1
  else
    self.t[self.insertIndex] = 0
  end

  -- are we overwriting the first episode right now?
  if #self.episodes > 0 and self.episodes[1].start <= self.insertIndex and self.episodes[1].finish >= self.insertIndex then
    table.remove(self.episodes, 1)
  end

  -- handle recording the episode
  if self.currentEpisodeStart < 0 then
    self.currentEpisodeStart = self.insertIndex
  elseif t then
    table.insert(self.episodes, { start = self.currentEpisodeStart, finish = self.insertIndex })
    self.currentEpisodeStart = -1
  end
end

-- seq_len: max length of sequence backwards to retrieve, starting from index
-- traverse_eps: allow traversal backwards into different episodes or not
-- index 1 is most recent, then backwards from there
function transition:get(index, max_seq_len, traverse_eps)
  traverse_eps = traverse_eps or false

  -- initialise the buffers if we need to
  if self.seq_s then
    self.seq_s:resize(max_seq_len, self.state_size + self.extra_state_size)
    self.seq_a:resize(max_seq_len)
    self.seq_r:resize(max_seq_len)
    self.seq_t:resize(max_seq_len)
  else
    self.seq_s = torch.ByteTensor(max_seq_len, self.state_size + self.extra_state_size)
    self.seq_a = torch.ByteTensor(max_seq_len)
    self.seq_r = torch.FloatTensor(max_seq_len)
    self.seq_t = torch.ByteTensor(max_seq_len)
  end

  self.seq_s:fill(0)
  self.seq_a:fill(0)
  self.seq_r:fill(0)
  self.seq_t:fill(0)

  -- iterate backwards until we hit a stopping point
  local seq_len = 0
  while seq_len < max_seq_len do
    -- increment the sequence length
    seq_len = seq_len + 1
    local ind = ((index - seq_len) % self.size) + 1 -- 1-base indexing is weird...
    if not traverse_eps and seq_len > 1 and (ind == self.insertIndex or self.t[ind] == 1) then
      seq_len = seq_len - 1 -- roll-back one step
      break
    end
    -- copy from transitions
    self.seq_s[max_seq_len - seq_len + 1]:copy(self.s[ind])
    self.seq_a[max_seq_len - seq_len + 1] = self.a[ind]
    self.seq_r[max_seq_len - seq_len + 1] = self.r[ind]
    self.seq_t[max_seq_len - seq_len + 1] = self.t[ind]
  end

  return seq_len, self.seq_s, self.seq_t, self.seq_a, self.seq_r
end

function transition:sample(batch_size, seq_len, buf_s, buf_t, buf_a, buf_r)
  -- initialise buffers for batch if they weren't provided
  buf_s = buf_s or torch.FloatTensor(batch_size, seq_len, self.state_size + self.extra_state_size)
  buf_a = buf_a or torch.FloatTensor(batch_size, seq_len)
  buf_r = buf_r or torch.FloatTensor(batch_size, seq_len)
  buf_t = buf_t or torch.FloatTensor(batch_size, seq_len)

  -- fill buffer
  local max_len = 0
  for i = 1, batch_size do
    local len, s, t, a, r = self:sample_one(seq_len)
    if len > max_len then max_len = len end
    buf_s[i]:copy(s)
    buf_a[i]:copy(a)
    buf_r[i]:copy(r)
    buf_t[i]:copy(t)
  end

  return max_len, buf_s, buf_t, buf_a, buf_r
end

function transition:sample_episodes(batch_size, seq_len, buf_s, buf_t, buf_a, buf_r)
  -- initialise buffers for batch if they weren't provided
  buf_s = buf_s or torch.FloatTensor(batch_size, seq_len, self.state_size + self.extra_state_size)
  buf_a = buf_a or torch.FloatTensor(batch_size, seq_len)
  buf_r = buf_r or torch.FloatTensor(batch_size, seq_len)
  buf_t = buf_t or torch.FloatTensor(batch_size, seq_len)

  -- fill buffer
  local max_len = 0
  for i = 1, batch_size do
    local len, s, t, a, r = self:sample_episode(seq_len)
    if len > max_len then max_len = len end
    buf_s[i]:copy(s)
    buf_a[i]:copy(a)
    buf_r[i]:copy(r)
    buf_t[i]:copy(t)
  end

  return max_len, buf_s, buf_t, buf_a, buf_r
end

function transition:sample_episode(max_seq_len)
  assert(#self.episodes > 1, 'Unable to sample from no episodes')

  local episode
  local valid = false
  while not valid do
    -- sample a random episode
    local ep_ind = torch.random(#self.episodes)
    episode = self.episodes[ep_ind]

    valid = true
    -- get the length
    local len = (episode.finish - episode.start) + 1
    if len < 0 then len = self.size + len end -- remember that len is negative
    -- length check
    if _hack_verbose then print(string.format('Sampled #%d [%d, %d] Length: %d', ep_ind, episode.start, episode.finish, len)) end
    if len > max_seq_len then
      if _hack_verbose then print('Discarded!') end
      valid = false
      self.n_discarded_samples = self.n_discarded_samples + 1
    end
  end
  self.n_samples = self.n_samples + 1
  return self:get(episode.finish, max_seq_len) -- use max_seq_len so we don't trigger the cache resize
end

function transition:sample_one(seq_len)
  assert(self.numEntries > 1, 'Unable to sample from nothing!')

  local index
  local valid = false
  while not valid do
    -- sample a random occupied index
    index = torch.random(self.numEntries)
    -- probabilistically discard non-event sequences
    valid = true
    if self.non_reward_prob > 0 and torch.uniform() < self.non_reward_prob then
        -- we will discard this sequence if it does not contain a reward
        valid = false
        for i = 1, seq_len do
          local seq_ind = ((index - i) % self.size) + 1
          if i > 1 and self.t[seq_ind] == 1 then break end -- break early if we hit the end of the episode
          if self.r[seq_ind] ~= 0 then valid = true end
        end

        -- sampling statistics
        if not valid then
          self.n_discarded_samples = self.n_discarded_samples + 1
        end
    end
  end
  self.n_samples = self.n_samples + 1
  return self:get(index, seq_len)
end

return transition
