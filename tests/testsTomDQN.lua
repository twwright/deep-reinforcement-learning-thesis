local luaunit = require 'luaunit.luaunit'
require 'cunn'

package.path = 'TomDQN/?.lua;' .. package.path

-- TransitionTable
local TomDQN = require 'TomDQN.TomDQN'

function tensorEquals(t1, t2)
  return torch.sum(torch.csub(t1, t2)) == 0
end

function setup(self)
  self.random_func = torch.random
  self.random_count = 0
  function stubRandom(range)
    self.random_count = (self.random_count % 8) + 1
    return self.random_count
  end
  torch.random = stubRandom

  self.uniform_func = torch.uniform
  self.uniform_count = 0
  function stubUniform()
    local randoms = {0.15, 0.3, 0.6, 0.85}
    self.uniform_count = ((self.uniform_count) % #randoms) + 1
    return randoms[self.uniform_count]
  end
  torch.uniform = stubUniform
end

function teardown(self)
  torch.random = self.random_func
  torch.uniform = self.uniform_func
end


TestCaching = {}

function TestCaching:testGetCache()
  -- create agent
  local agent = TomDQN:new({ model = nn.Linear(2, 2):float(), actions = {'a1', 'a2'}, state_size = 2, size = 8})

  -- get a cache that doesn't yet exist
  local c = agent:get_cache({2, 3}, 'test', false)

  -- test the size
  luaunit.assertEquals(c:size():totable(), {2, 3})

  -- retrieve the cache we created again
  local c2 = agent:get_cache({2, 3}, 'test', false)

  -- test that the cache is the same
  luaunit.assertIs(c, c2)
end

function TestCaching:testCacheCopy()
  -- create agent
  local agent = TomDQN:new({ model = nn.Linear(2, 2):float(), actions = {'a1', 'a2'}, state_size = 2, size = 8})

  -- copy to a cache that doesn't exist
  local data = torch.FloatTensor({1, 2, 3})
  local c = agent:cache_copy(data, 'test', false)
  luaunit.assertTrue(tensorEquals(c, data)) -- test that copy worked correctly

  -- retrieve the cache we created again
  local c2 = agent:get_cache(data, 'test', false)
  luaunit.assertIs(c, c2) -- test that the cache is correct
  luaunit.assertTrue(tensorEquals(c2, data))

  -- copy new data to existing cache
  local data2 = torch.FloatTensor({4, 5, 6})
  local c3 = agent:cache_copy(data2, 'test', false)
  luaunit.assertIs(c, c3) -- check that it is the same cache
  luaunit.assertTrue(tensorEquals(c3, data2)) -- but check the new data is copied in
end

function TestCaching:testGPU()
  -- create agent that is GPU enabled
  local agent = TomDQN:new({ model = nn.Linear(2, 2):float(), actions = {'a1', 'a2'}, state_size = 2, size = 8, gpu = 1})

  local data = torch.FloatTensor({1, 2, 3})

  -- create a new GPU cache
  local gpu_c = agent:get_cache(data, 'test_gpu', true)
  luaunit.assertEquals(torch.type(gpu_c), 'torch.CudaTensor')

  -- create a new non-GPU cache
  local nogpu_c = agent:get_cache(data, 'test_no_gpu', false)
  luaunit.assertEquals(torch.type(nogpu_c), 'torch.FloatTensor')

  -- recreate agent as non-GPU agent
  agent = TomDQN:new({ model = nn.Linear(2, 2):float(), actions = {'a1', 'a2'}, state_size = 2, size = 8})

  -- attempt to create a GPU cache, should fallback to non-GPU
  gpu_c = agent:get_cache(data, 'test_gpu', true)
  luaunit.assertEquals(torch.type(gpu_c), 'torch.FloatTensor')

  -- create a new non-GPU cache
  nogpu_c = agent:get_cache(data, 'test_no_gpu', false)
  luaunit.assertEquals(torch.type(nogpu_c), 'torch.FloatTensor')
end

function TestCaching:testResize()
  -- create agent
  agent = TomDQN:new({ model = nn.Linear(2, 2):float(), actions = {'a1', 'a2'}, state_size = 2, size = 8})

  -- create data
  local data = torch.FloatTensor({1, 2, 3})
  local data2 = torch.FloatTensor({{1, 2, 3}, {4, 5, 6}})

  -- create cache
  local c = agent:cache_copy(data, 'test', false)
  luaunit.assertTrue(tensorEquals(data, c))

  -- retrieve same cache, copy different size data
  c = agent:cache_copy(data2, 'test', false)
  luaunit.assertTrue(tensorEquals(data2, c))
end

TestCalcDelta = {}

function TestCalcDelta:setup()
  setup(self)
end

function TestCalcDelta:teardown()
  teardown(self)
end

function TestCalcDelta:testCalcDelta()
  -- create agent
  local agent = TomDQN:new({ model = nn.Linear(2, 2):float(), actions = {'a1', 'a2'}, state_size = 2, size = 8})

  -- create some data to use
  local s = torch.ByteTensor({
    {0, 0}, {0, 1}, {1, 0}, {1, 1}, {1, 1}, {1, 0}, {0, 1}, {0, 0}
  })
  local a = torch.ByteTensor({1, 2, 1, 0, 2, 1, 2, 0})
  local t_tensor = torch.ByteTensor({0, 0, 0, 1, 0, 0, 0, 1})
  local t = { false, false, false, true, false, false, false, true }
  local r = torch.FloatTensor({0, 0, 0, -0.5, 0, 0.5, 0, 1})

  -- add the data to the transition table
  for i = 1, s:size(1) do
    agent.tt:add(s[i], t[i], a[i], r[i])
  end

  -- sample
  local b_s, b_t, b_a, b_r = agent.tt:sample(6, 2)

  --print(b_s)
  local targets, deltas, q2_max = agent:calc_deltas(b_s, b_t, b_a, b_r)

  --print(targets)
  --print(deltas)
end

TestModel = {}

function TestModel:testModel()
  local opt = {
    input_dims = {1, 15, 15},
    model_layers = {{32, 6, 1}, {64, 4, 1}, {64, 3, 1}},
    actions = {'a1', 'a2', 'a3', 'a4'}
  }
  local model_func = require('model_dqn')
  local model = model_func(opt):float()
  --print(model)
  --print(model:forward(torch.zeros(225):float()))
end
-- End TestModel

Test01Caching = TestCaching
Test02CalcDelta = TestCalcDelta
Test03Model = TestModel

TestCaching = nil
TestCalcDelta = nil
TestModel = nil

luaunit.LuaUnit.run()
