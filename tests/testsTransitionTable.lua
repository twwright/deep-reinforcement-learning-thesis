local luaunit = require 'luaunit.luaunit'

-- TransitionTable
local TransitionTable = require 'TomDQN.TomTransitionTable'

function tensorEquals(t1, t2)
  return torch.sum(torch.csub(t1, t2)) == 0
end

function setup(self)
  self.random_func = torch.random
  self.random_count = 0
  function stubRandom(range)
    local randoms = {1, math.ceil(range / 2), range}
    self.random_count = ((self.random_count) % 3) + 1
    return randoms[self.random_count]
  end
  torch.random = stubRandom

  self.uniform_func = torch.uniform
  self.uniform_count = 0
  function stubUniform()
    local randoms = {0.15, 0.3, 0.6, 0.85}
    self.uniform_count = ((self.uniform_count) % #randoms) + 1
    return randoms[self.uniform_count]
  end
  torch.uniform = stubUniform
end

function teardown(self)
  torch.random = self.random_func
  torch.uniform = self.uniform_func
end

-- Test creating a transition table
local TestCreate = {}

function TestCreate:testNew()
  local tt = TransitionTable:new({
    state_size = 3,
    size = 5,
    non_reward_prob = 0.1
  })

  -- test storage sizes
  luaunit.assertEquals(tt.s:size():totable(), { 5, 3})
  luaunit.assertEquals(tt.a:size():totable(), { 5 })
  luaunit.assertEquals(tt.r:size():totable(), { 5 })
  luaunit.assertEquals(tt.t:size():totable(), { 5 })

  -- test state variables
  luaunit.assertEquals(tt.numEntries, 0)
  luaunit.assertEquals(tt.size, 5)
  luaunit.assertEquals(tt.insertIndex, 0)
end

function TestCreate:testTensorEquals()
  local t1 = torch.ones(10)
  local t2 = torch.ones(10)

  luaunit.assertTrue(tensorEquals(t1, t2))
  t1[1] = 0
  luaunit.assertFalse(tensorEquals(t1, t2))
end
-- End TestCreate

-- Test functionality of adding to the transition table
local TestAdd = {}

function TestAdd:testAddFromEmpty()
  local tt = TransitionTable:new({
    state_size = 3,
    size = 5,
    non_reward_prob = 0.1
  })

  -- add the initial state
  local s = torch.ByteTensor({{1, 0, 0}})
  tt:add(s, false, 0, 0)

  -- check state variables
  luaunit.assertEquals(tt.numEntries, 1)
  luaunit.assertEquals(tt.insertIndex, 1)

  -- check state added correctly
  luaunit.assertTrue(tensorEquals(tt.s[1], s))
  luaunit.assertEquals(tt.t[1], 0)
  luaunit.assertEquals(tt.a[1], 0)
  luaunit.assertEquals(tt.r[1], 0)
end

function TestAdd:testAddFromNotEmpty()
  local tt = TransitionTable:new({
    state_size = 3,
    size = 5,
    non_reward_prob = 0.1
  })

  -- create the transitions data
  local s = torch.ByteTensor({
    {1, 0, 0}, {0, 1, 0}, {0, 0, 1}, {1, 0, 0}, {1, 0, 0}
  })
  local a = torch.ByteTensor({0, 1, 2, 1, 0})
  local t_tensor = torch.ByteTensor({0, 0, 0, 0, 1})
  local t = { false, false, false, false, true }
  local r = torch.FloatTensor({0, 0, 0, -0.5, 1})

  -- add some transitions (s, t, a, r)
  tt:add(s[1], t[1], a[1], r[1]) -- initial state
  tt:add(s[2], t[2], a[2], r[2]) -- transition to 010, not terminal, action 1, no reward
  tt:add(s[3], t[3], a[3], r[3]) -- transition to 001, not terminal, action 2, no reward
  tt:add(s[4], t[4], a[4], r[4]) -- transition to 100, not terminal, action 1, -0.5 reward
  tt:add(s[5], t[5], a[5], r[5]) -- transition to 100, terminal, no action, 1 reward

  -- check state variables
  luaunit.assertEquals(tt.numEntries, 5)
  luaunit.assertEquals(tt.insertIndex, 5)

  -- check transitions added correctly
  luaunit.assertTrue(tensorEquals(tt.s, s))
  luaunit.assertTrue(tensorEquals(tt.t, t_tensor))
  luaunit.assertTrue(tensorEquals(tt.a, a))
  luaunit.assertTrue(tensorEquals(tt.r, r))
end

function TestAdd:testAddFromFull()
  -- create the transition table
  local tt = TransitionTable:new({
    state_size = 3,
    size = 5,
    non_reward_prob = 0.1
  })

  -- create the transitions data
  local s = torch.ByteTensor({
    {1, 0, 0}, {0, 1, 0}, {0, 0, 1}, {1, 0, 0}, {1, 0, 0}
  })
  local a = torch.ByteTensor({0, 1, 2, 1, 0})
  local t_tensor = torch.ByteTensor({0, 0, 0, 0, 1})
  local t = { false, false, false, false, true }
  local r = torch.FloatTensor({0, 0, 0, -0.5, 1})

  -- add the transitions to fill the table
  for i = 1, 5 do tt:add(s[i], t[i], a[i], r[i]) end

  -- change first two transitions, add them
  s[1]:copy(torch.ByteTensor({{0, 0, 1}}))
  a[1] = 3
  t_tensor[1] = 0
  t[1] = false
  r[1] = 0
  tt:add(s[1], t[1], a[1], r[1])
  s[2]:copy(torch.ByteTensor({{0, 1, 0}}))
  a[2] = 4
  t_tensor[2] = 0
  t[2] = false
  r[2] = 1
  tt:add(s[2], t[2], a[2], r[2])

  -- check state variables
  luaunit.assertEquals(tt.numEntries, 5)
  luaunit.assertEquals(tt.insertIndex, 2)

  -- check contents
  luaunit.assertTrue(tensorEquals(tt.s, s))
  luaunit.assertTrue(tensorEquals(tt.t, t_tensor))
  luaunit.assertTrue(tensorEquals(tt.a, a))
  luaunit.assertTrue(tensorEquals(tt.r, r))
end
-- End TestAdd

local TestGet = {}

-- test retrieving a full sequence that doesn't cross over into a different episode
function TestGet:testGet()
  -- create a transition table to use
  local tt = TransitionTable:new({
    state_size = 2,
    size = 8,
    non_reward_prob = 0.1
  })

  -- create some data to use
  local s = torch.ByteTensor({
    {0, 0}, {0, 1}, {1, 0}, {1, 1}, {1, 1}, {1, 0}, {0, 1}, {0, 0}
  })
  local a = torch.ByteTensor({1, 2, 3, 0, 4, 3, 2, 0})
  local t_tensor = torch.ByteTensor({0, 0, 0, 1, 0, 0, 0, 1})
  local t = { false, false, false, true, false, false, false, true }
  local r = torch.FloatTensor({0, 0, 0, -0.5, 0, 0.5, 0, 1})

  -- add the data to the transition table
  for i = 1, s:size(1) do
    tt:add(s[i], t[i], a[i], r[i])
  end

  -- retrieve various sequences and test them
  local tests = {{8, 4}, {4, 4}, {7, 3}, {4, 3}}
  for i = 1, #tests do
    local ind, len = unpack(tests[i])
    local seq_len, seq_s, seq_t, seq_a, seq_r = tt:get(ind, len)
    for j = 1, seq_len do
      luaunit.assertEquals(seq_len, len)
      luaunit.assertTrue(tensorEquals(seq_s[j], s[ind-len+j]))
      luaunit.assertEquals(seq_t[j], t_tensor[ind-len+j])
      luaunit.assertEquals(seq_a[j], a[ind-len+j])
      luaunit.assertEquals(seq_r[j], r[ind-len+j])
    end
  end
end

-- test retrieving a sequence that wraps around the boundary
function TestGet:testGetAcrossBoundary()
  -- create a transition table to use
  local tt = TransitionTable:new({
    state_size = 2,
    size = 8,
    non_reward_prob = 0.1
  })

  -- create some data to use
  local s = torch.ByteTensor({
    {0, 0}, {0, 1}, {1, 0}, {1, 1}, {1, 1}, {1, 0}, {0, 1}, {0, 0}
  })
  local a = torch.ByteTensor({1, 2, 3, 0, 4, 3, 2, 0})
  local t_tensor = torch.ByteTensor({0, 0, 0, 1, 0, 0, 0, 1})
  local t = { false, false, false, true, false, false, false, true }
  local r = torch.FloatTensor({0, 0, 0, -0.5, 0, 0.5, 0, 1})

  -- add the first transition twice to offset the two episodes of length 4
  tt:add(s[1], t[1], a[1], r[1])
  tt:add(s[1], t[1], a[1], r[1])
  -- add the data to the transition table
  for i = 1, s:size(1) do
    tt:add(s[i], t[i], a[i], r[i])
  end

  -- retrieve various sequences and test them
  local tests = {{2, 4}, {1, 3}}
  for i = 1, #tests do
    local ind, len = unpack(tests[i])
    local seq_len, seq_s, seq_t, seq_a, seq_r = tt:get(ind, len)
    for j = 1, seq_len do
      local check_ind = (ind-len+j-3) % s:size(1) + 1 -- minus 3 to correct for offset of 2
      luaunit.assertEquals(seq_len, len)
      luaunit.assertTrue(tensorEquals(seq_s[j], s[check_ind]))
      luaunit.assertEquals(seq_t[j], t_tensor[check_ind])
      luaunit.assertEquals(seq_a[j], a[check_ind])
      luaunit.assertEquals(seq_r[j], r[check_ind])
    end
  end
end

-- test retrieving from a point where the sequence collides with the previous episode
function TestGet:testGetPartialSequence()
  -- create a transition table to use
  local tt = TransitionTable:new({
    state_size = 2,
    size = 8,
    non_reward_prob = 0.1
  })

  -- create some data to use
  local s = torch.ByteTensor({
    {0, 0}, {0, 1}, {1, 0}, {1, 1}, {1, 1}, {1, 0}, {0, 1}, {0, 0}
  })
  local a = torch.ByteTensor({1, 2, 3, 0, 4, 3, 2, 0})
  local t_tensor = torch.ByteTensor({0, 0, 0, 1, 0, 0, 0, 1})
  local t = { false, false, false, true, false, false, false, true }
  local r = torch.FloatTensor({0, 0, 0, -0.5, 0, 0.5, 0, 1})

  -- add the data to the transition table
  for i = 1, s:size(1) do
    tt:add(s[i], t[i], a[i], r[i])
  end

  -- retrieve various sequences and test them
  -- {index, length to retrieve, actual length that will be retrieved}
  local tests = {{6, 4, 2}, {4, 6, 4}, {8, 6, 4}}
  for i = 1, #tests do
    local ind, len, actual_len = unpack(tests[i])
    local seq_len, seq_s, seq_t, seq_a, seq_r = tt:get(ind, len)
    for j = len - seq_len + 1, len do
      local check_ind = (ind-len+j-1) % s:size(1) + 1
      luaunit.assertEquals(seq_len, actual_len)
      luaunit.assertTrue(tensorEquals(seq_s[j], s[check_ind]))
      luaunit.assertEquals(seq_t[j], t_tensor[check_ind])
      luaunit.assertEquals(seq_a[j], a[check_ind])
      luaunit.assertEquals(seq_r[j], r[check_ind])
    end
  end
end
-- End TestGet

local TestSampleOne = {}
function TestSampleOne:setup()
  setup(self)
end
function TestSampleOne:teardown()
  setup(self)
end
function TestSampleOne:testRandomStub()
  luaunit.assertEquals(torch.random(4), 1)
  luaunit.assertEquals(torch.random(4), 2)
  luaunit.assertEquals(torch.random(4), 4)
  luaunit.assertEquals(torch.random(5), 1)
  luaunit.assertEquals(torch.random(5), 3)
  luaunit.assertEquals(torch.random(5), 5)
end

function TestSampleOne:testUniformStub()
  luaunit.assertEquals(torch.uniform(), 0.15)
  luaunit.assertEquals(torch.uniform(), 0.3)
  luaunit.assertEquals(torch.uniform(), 0.6)
  luaunit.assertEquals(torch.uniform(), 0.85)
  luaunit.assertEquals(torch.uniform(), 0.15)
  luaunit.assertEquals(torch.uniform(), 0.3)
  luaunit.assertEquals(torch.uniform(), 0.6)
  luaunit.assertEquals(torch.uniform(), 0.85)
end

-- test standard sample
function TestSampleOne:testSample()
  -- create a transition table to use
  local tt = TransitionTable:new({
    state_size = 2,
    size = 8,
    non_reward_prob = 0
  })

  -- create some data to use
  local s = torch.ByteTensor({
    {0, 0}, {0, 1}, {1, 0}, {1, 1}, {1, 1}, {1, 0}, {0, 1}, {0, 0}
  })
  local a = torch.ByteTensor({1, 2, 3, 0, 4, 3, 2, 0})
  local t_tensor = torch.ByteTensor({0, 0, 0, 1, 0, 0, 0, 1})
  local t = { false, false, false, true, false, false, false, true }
  local r = torch.FloatTensor({0, 0, 0, -0.5, 0, 0.5, 0, 1})

  -- add the data to the transition table
  for i = 1, s:size(1) do
    tt:add(s[i], t[i], a[i], r[i])
  end

  -- retrieve various sequences and test them
  self.random_count = 0
  self.uniform_count = 0
  local tests = {1, 4, 8}
  for i = 1, #tests do
    local ind = tests[i]
    local len = 3
    local seq_len, seq_s, seq_t, seq_a, seq_r = tt:sample_one(len)
    for j = len - seq_len + 1, len do
      local check_ind = (ind-len+j-1) % s:size(1) + 1
      luaunit.assertTrue(tensorEquals(seq_s[j], s[check_ind]))
      luaunit.assertEquals(seq_t[j], t_tensor[check_ind])
      luaunit.assertEquals(seq_a[j], a[check_ind])
      luaunit.assertEquals(seq_r[j], r[check_ind])
    end
  end
end

-- test discarding an episode and sampling another
function TestSampleOne:testProbabilisticSample()
  -- create a transition table to use
  local tt = TransitionTable:new({
    state_size = 2,
    size = 8,
    non_reward_prob = 0.4
  })

  -- create some data to use
  local s = torch.ByteTensor({
    {0, 0}, {0, 1}, {1, 0}, {1, 1}, {1, 1}, {1, 0}, {0, 1}, {0, 0}
  })
  local a = torch.ByteTensor({1, 2, 3, 0, 4, 3, 2, 0})
  local t_tensor = torch.ByteTensor({0, 0, 0, 1, 0, 0, 0, 1})
  local t = { false, false, false, true, false, false, false, true }
  local r = torch.FloatTensor({0, 0, 0, -0.5, 0, 0.5, 0, 1})

  -- add the data to the transition table
  for i = 1, s:size(1) do
    tt:add(s[i], t[i], a[i], r[i])
  end

  -- retrieve various sequences and test them
  self.random_count = 0
  self.uniform_count = 0

  local tests = {
    4, -- (1, 0.15) checked and discarded, (4, 0.3) checked and kept
    8, -- (8, 0.6) not-checked
    1, -- (1, 0.85) not-checked
    4, -- (4, 0.3) checked and kept
    8, -- (8, 0.6) not-checked
    1  -- (1, 0.85) not-checked
  }
  for i = 1, #tests do
    local ind = tests[i]
    local len = 3
    local seq_len, seq_s, seq_t, seq_a, seq_r = tt:sample_one(len)
    for j = len - seq_len + 1, len do
      local check_ind = (ind-len+j-1) % s:size(1) + 1
      luaunit.assertTrue(tensorEquals(seq_s[j], s[check_ind]))
      luaunit.assertEquals(seq_t[j], t_tensor[check_ind])
      luaunit.assertEquals(seq_a[j], a[check_ind])
      luaunit.assertEquals(seq_r[j], r[check_ind])
    end
  end
end

-- End TestSampleOne

local TestSample = {}
function TestSample:setup()
  setup(self)
end
function TestSample:teardown()
  setup(self)
end

-- test that a batch is correctly sampled
function TestSample:testSample()
  -- create a transition table to use
  local tt = TransitionTable:new({
    state_size = 2,
    size = 8,
    non_reward_prob = 0
  })

  -- create some data to use
  local s = torch.FloatTensor({
    {0, 0}, {0, 1}, {1, 0}, {1, 1}, {1, 1}, {1, 0}, {0, 1}, {0, 0}
  })
  local a = torch.ByteTensor({1, 2, 3, 0, 4, 3, 2, 0})
  local t_tensor = torch.ByteTensor({0, 0, 0, 1, 0, 0, 0, 1})
  local t = { false, false, false, true, false, false, false, true }
  local r = torch.FloatTensor({0, 0, 0, -0.5, 0, 0.5, 0, 1})

  -- add the data to the transition table
  for i = 1, s:size(1) do
    tt:add(s[i], t[i], a[i], r[i])
  end

  -- retrieve various sequences and test them
  self.random_count = 0
  self.uniform_count = 0
  local batch_indx = {{1, 1}, {4, 3}, {8, 3}}
  local len = 3
  local batch_s, batch_t, batch_a, batch_r = tt:sample(4, len)
  for i = 1, #batch_indx do
    local ind, seq_len = unpack(batch_indx[i])
    for j = len - seq_len + 1, len do
      local check_ind = (ind-len+j-1) % s:size(1) + 1
      luaunit.assertTrue(tensorEquals(batch_s[i][j], s[check_ind]))
      luaunit.assertEquals(batch_t[i][j], t_tensor[check_ind])
      luaunit.assertEquals(batch_a[i][j], a[check_ind])
      luaunit.assertEquals(batch_r[i][j], r[check_ind])
    end
  end
end

-- test that when buffers are provided they are used
function TestSample:testBuffersProvided()
  -- create a transition table to use
  local tt = TransitionTable:new({
    state_size = 2,
    size = 8,
    non_reward_prob = 0
  })

  -- create some data to use
  local s = torch.ByteTensor({
    {0, 0}, {0, 1}, {1, 0}, {1, 1}, {1, 1}, {1, 0}, {0, 1}, {0, 0}
  })
  local a = torch.ByteTensor({1, 2, 3, 0, 4, 3, 2, 0})
  local t_tensor = torch.ByteTensor({0, 0, 0, 1, 0, 0, 0, 1})
  local t = { false, false, false, true, false, false, false, true }
  local r = torch.FloatTensor({0, 0, 0, -0.5, 0, 0.5, 0, 1})

  -- add the data to the transition table
  for i = 1, s:size(1) do
    tt:add(s[i], t[i], a[i], r[i])
  end

  -- create buffers
  local buf_s = torch.ByteTensor(4, 3, 2)
  local buf_a = torch.ByteTensor(4, 3)
  local buf_r = torch.FloatTensor(4, 3)
  local buf_t = torch.ByteTensor(4, 3)

  -- retrieve various sequences and test them
  self.random_count = 0
  self.uniform_count = 0
  local tests = {1, 4, 8}
  for i = 1, #tests do
    local ind = tests[i]
    local seq_s, seq_t, seq_a, seq_r = tt:sample(4, 3, buf_s, buf_t, buf_a, buf_r)
    luaunit.assertIs(seq_s, buf_s)
    luaunit.assertIs(seq_t, buf_t)
    luaunit.assertIs(seq_a, buf_a)
    luaunit.assertIs(seq_r, buf_r)
  end
end
-- End TestSample

local TestSampleEpisodes = {}
function TestSampleEpisodes:setup()
  setup(self)
end
function TestSampleEpisodes:teardown()
  setup(self)
end

-- test that episode index is correctly added as transitions are added
function TestSampleEpisodes:testEpisodeRecording()
  -- create a transition table to use
  local tt = TransitionTable:new({
    state_size = 2,
    size = 8,
    non_reward_prob = 0
  })

  -- create some data to use
  local s = torch.ByteTensor({
    {0, 0}, {0, 1}, {1, 0}, {1, 1}, {1, 1}, {1, 0}, {0, 1}, {0, 0}, {1, 1}, {1, 0}, {0, 1}, {0, 0}
  })
  local a = torch.ByteTensor({1, 2, 3, 0, 4, 3, 2, 0, 1, 2, 3, 0, })
  local t_tensor = torch.ByteTensor({0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1 })
  local t = { false, false, false, true, false, false, false, true, false, false, false, true }
  local r = torch.FloatTensor({0, 0, 0, -0.5, 0, 0.5, 0, 1, 0, 0.5, 0, 1})

  -- add the first 8 elements of data to the transition table
  for i = 1, 8 do
    tt:add(s[i], t[i], a[i], r[i])
  end

  -- check that episodes recorded correctly!
  luaunit.assertEquals(#tt.episodes, 2)
  luaunit.assertEquals(tt.episodes[1].start, 1)
  luaunit.assertEquals(tt.episodes[1].finish, 4)
  luaunit.assertEquals(tt.episodes[2].start, 5)
  luaunit.assertEquals(tt.episodes[2].finish, 8)

  -- add the last 4 elements of data
  for i = 9, 12 do
    tt:add(s[i], t[i], a[i], r[i])
  end

  -- check that episodes recorded correctly! (first should have been deleted and replaced)
  luaunit.assertEquals(#tt.episodes, 2)
  luaunit.assertEquals(tt.episodes[2].start, 1)
  luaunit.assertEquals(tt.episodes[2].finish, 4)
  luaunit.assertEquals(tt.episodes[1].start, 5)
  luaunit.assertEquals(tt.episodes[1].finish, 8)

  -- sample episodes
end
-- End TestSampleEpisodes

Test01Create = TestCreate
Test02Add = TestAdd
Test03Get = TestGet
Test04SampleOne = TestSampleOne
Test05Sample = TestSample
Test06SampleEpisodes = TestSampleEpisodes

luaunit.LuaUnit.run()
