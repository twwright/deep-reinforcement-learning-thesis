require 'nn'
require 'cunn'
require 'rnn'

function create_model(opt)
  local net = torch.load(opt.model_file)
  if net.forget then net:forget() end
  net:clearState()
  return net
end

return create_model
