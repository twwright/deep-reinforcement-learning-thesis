require 'nn'
require 'cunn'

function create_model(opt)
  local net = nn.Sequential()

  -- simple linear model
  local size = 1
  for i = 1, #opt.input_dims do
    size = size * opt.input_dims[i]
  end
  net:add(nn.Linear(size, #opt.actions))

  return net
end

return create_model
