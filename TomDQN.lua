require 'optim'

require 'deepmind_rmsprop'

local TransitionTable = require('TomTransitionTable')

local dqn = {}

function dqn:new(opt)
  opt = opt or {}
  -- assert that we have some stuff
  assert(opt.model, 'Model must be provided!')
  assert(opt.actions, 'Actions must be provided!')
  -- set parameters
  local obj = {
    preproc_func = opt.preproc_func,
    train_network = opt.model,
    eval_network = opt.model:clone(),
    target_network = opt.model:clone(),
    actions = opt.actions,
    -- q-learning parameters,
    discount_gamma = opt.discount_gamma or 0.99,
    sample_episodes = opt.sample_episodes,
    adaptive_seq = opt.adaptive_seq,
    sarsa = opt.sarsa, -- use SARSA learning rather than Q-learning?
    qsarsa = opt.qsarsa, -- use Q-SARSA learning?
    sigma_start_steps = opt.sigma_start_steps or 500000,
    sigma_steps = opt.sigma_steps or 50000,
    sigma_start = opt.sigma_start or 1.0,
    sigma_end = opt.sigma_end or 0,
    ddqn = opt.ddqn, -- use Double DQN? van Hasselt (2015)
    single_step_bp = opt.single_step_bp, -- only perform single backward pass on RNN training? (Kaparthy tweet?)
    rho = opt.rho, -- sequence length for BPTT
    advantage_learning = opt.advantage_learning, -- utilise Advantage learning instead of Q-learning?
    advantage_kappa = opt.advantage_kappa or 0.1, -- kappa constant for Advantage learning
    -- training parameters
    train_freq = opt.train_freq or 4, -- number of steps between training updates
    train_batches = opt.train_batches or 1, -- number of minibatches to train each update
    batch_size = opt.batch_size or 128, -- size of training minibatch
    target_q_freq = opt.target_q_freq or 10000, -- steps between snapshotting model as target
    clip_delta = opt.clip_delta or 1, -- clamp bounds for targets
    clip_gradient = opt.clip_gradient, -- clamp bounds for gradients
    optim_method = optim[opt.optim_method] or optim.deepmind_rmsprop,
    optim_state = opt.optim_state or {
      learningRate = 0.00025, -- RMSprop learning rate from DeepMind DQN
      alpha = 0.95, -- RMSprop momentum from DeepMind DQN
      epsilon = 0.01 -- RMSprop gradient constant from DeepMind DQN
    },
    -- reward parameters for reward processing
    min_reward = opt.min_reward or -1,
    max_reward = opt.max_reward or 1,
    rescale_r = opt.rescale_r or 1,
    r_max = 1,
    -- epsilon parameters for e-greedy policy
    ep_start = opt.ep_start or 1.0, -- initial value of ep
    ep_end = opt.ep_end or 0.1, -- final value of ep
    ep_steps = opt.ep_steps or 1000000, -- num steps to linearly anneal ep over
    ep_test = opt.ep_test or 0.05, -- ep to use during evaluation
    -- create the transition table
    tt = TransitionTable:new(opt),
    transition_store_func = opt.transition_store_func,
    transition_retrieve_func = opt.transition_retrieve_func,
    -- gpu acceleration
    gpu = opt.gpu,
    cache = {} -- table to store cache tensors
  }
  -- set network states correctly
  obj.train_network:training()
  obj.target_network:evaluate()
  obj.eval_network:evaluate()
  if obj.gpu then
    obj.train_network:cuda()
    obj.target_network:cuda()
  end

  -- if using Double DQN and we're training on sequences, create a new network clone -- we need a separate one because of internal state
  if obj.ddqn and opt.rho then
    obj.ddqn_network = obj.train_network:clone()
    obj.ddqn_network:evaluate()
    obj.ddqn_w, _ = obj.ddqn_network:getParameters()
  end

  -- hook into the model parameters
  obj.w, obj.dw = obj.train_network:getParameters()
  obj.eval_w, _ = obj.eval_network:getParameters()
  setmetatable(obj, self)
  self.__index = self
  return obj
end

function dqn:preprocess(s, r)
  -- transform state using the preprocess function
  if self.preproc_func then
    s = self.preproc_func(s)
  end
  s = s:reshape(self.tt.state_size)

  -- clamp reward
  if self.min_reward then
    r = math.max(r, self.min_reward)
  end
  if self.max_reward then
    r = math.min(r, self.max_reward)
  end

  return s, r
end

function dqn:eGreedy(s, ep)
  if torch.uniform() < ep then
    if self.rho then self:greedy(s) end -- if the network has state then we need to still evaluate to stay up-to-date!
    return torch.random(1, #self.actions)
  else
    return self:greedy(s)
  end
end

function dqn:get_cache(t, cache_field, is_gpu_cache)
  -- handle creating the cache if it doesn't exist
  if not self.cache[cache_field] then
    self.cache[cache_field] = torch.FloatTensor()
    if is_gpu_cache and self.gpu then
      self.cache[cache_field] = self.cache[cache_field]:cuda()
    end
  end

  -- resize the cache if necessary
  if torch.isTensor(t) then
    if self.cache[cache_field]:nElement() ~= t:nElement() then
      self.cache[cache_field]:resize(t:size()) -- t is tensor, so resize to match
    end
  else
    self.cache[cache_field]:resize(unpack(t)) -- t is sizes, so use resize
  end

  return self.cache[cache_field]
end

function dqn:cache_copy(t, cache_field, is_gpu_cache)
  -- get the cache
  local cache_t = self:get_cache(t, cache_field, is_gpu_cache)

  -- copy contents into the cache
  cache_t:copy(t)

  -- return the cache reference
  return cache_t
end

function dqn:greedy(s)
  -- forward pass to get q-values from s
  local q = self.eval_network:forward(s):squeeze()
  -- evaluate all actions to determine the best ones
  local max_q = q[1]
  local best_a = { 1 }
  for a = 2, #self.actions do
    if q[a] > max_q then
      max_q = q[a]
      best_a = { a }
    elseif q[a] == max_q then
      best_a[#best_a + 1] = a
    end
  end

  return best_a[torch.random(1, #best_a)] -- random tie-breaking if necessary
end

function dqn:calc_ep(curr_steps)
  local steps = math.min(curr_steps, self.ep_steps)  -- clamp the number of steps to max bound
  local ep_range = self.ep_start - self.ep_end -- difference between start and end, we are interpolating this
  local alpha = (self.ep_steps - steps) / self.ep_steps -- how much of ep_range are we adding?
  return self.ep_end + (ep_range * alpha)
end

function dqn:calc_sigma(curr_steps)
  local steps = math.min(math.max(0, curr_steps - self.sigma_start_steps), self.sigma_steps)  -- clamp the number of steps to max bound
  local sigma_range = self.sigma_start - self.sigma_end -- difference between start and end, we are interpolating this
  local alpha = (self.sigma_steps - steps) / self.sigma_steps -- how much of sigma_range are we adding?
  return self.sigma_end + (sigma_range * alpha)
end

function dqn:run(start_steps, steps, game_env, ep, train, eval, transition_store)
  -- initialise some counts for statistics
  local n_episodes = 1
  local n_rewards_pos = 0
  local n_rewards_neg = 0
  local reward_sum = 0
  -- start a new game
  self.tt:reset_episode()
  if self.rho then self.eval_network:forget() end
  local s, r, t = game_env:newGame()
  local a_ind = 0 -- no action taken thus far
  -- iterate training steps
  for step = start_steps, start_steps + steps do
    -- let the agent process the raw state and current reward
    s, r = self:preprocess(s, r)
    -- select an action using e-greedy
    if t then
      -- do not select an action as we are in a terminal state, and reset the network state
      a_ind = 0
      if self.rho then self.eval_network:forget() end
    else
      local step_ep = ep or self:calc_ep(step)
      a_ind = self:eGreedy(s, step_ep)
    end

    -- store information for this timestep in transition table
    if transition_store then
      if self.transition_store_func then
        s = self:transition_store_func(s)
      end
      self.tt:add(s, t, a_ind, r)
    end

    if t then
      -- game over, lets start a new one
      s, r, t = game_env:newGame()
      if eval then n_episodes = n_episodes + 1 end
    else
      -- perform action in game (training mode), observe the result
      s, r, t = game_env:step(self.actions[a_ind], train)
    end

    -- train?
    if train then
        if step % self.train_freq == 0 then
          for i = 1, self.train_batches do
            -- train using RMSprop from optim
            --local _, loss = optim.rmsprop(self.optim_eval, self.w, self.optim_state)
            -- train using RMSprop from DeepMind's DQN
            if self.qsarsa then
              self.sigma = self:calc_sigma(step)
            end
            local _, loss = self.optim_method(self.optim_eval, self.w, self.optim_state)
            self.eval_w:copy(self.w) -- synchronise evaluation and training models this way
            if self.ddqn_w then -- if we have a separate DDQN network, need to keep it synchronised
              self.ddqn_w:copy(self.w)
            end
          end
        end

        -- update target Q network
        if step % self.target_q_freq == 0 then
          self.target_network = self.train_network:clone()
          self.target_network:evaluate()
        end
     end

     -- capture reward statistics
     if eval and r ~= 0 then
         reward_sum = reward_sum + r
         if r > 0 then
             n_rewards_pos = n_rewards_pos + 1
         else
             n_rewards_neg = n_rewards_neg + 1
         end
     end
  end

  return n_episodes, n_rewards_pos, n_rewards_neg, reward_sum
end

function dqn:train(curr_steps, steps, game_env)
  return self:run(curr_steps, steps, game_env, nil, true, true, true)
end

function dqn:evaluate(steps, game_env, ep)
  return self:run(1, steps, game_env, ep or self.ep_test, false, true, false)
end

function dqn:calc_validation(valid_s, valid_t, valid_a, valid_r)
  assert(valid_s:size(1) % self.batch_size == 0, 'validation set must be multiple of batch size!')
  local batches = valid_s:size(1) / self.batch_size
  local v = 0
  local delta = 0
  for i = 1, batches do
    local selector = {(i-1) * self.batch_size + 1, i * self.batch_size}
    local b_valid_s, b_valid_t, b_valid_a, b_valid_r = valid_s[{selector}], valid_t[{selector}], valid_a[{selector}] , valid_r[{selector}]
    local batch_deltas, batch_v
    if self.rho then
      _, batch_deltas, batch_v = self:calc_targets_seq(b_valid_s, b_valid_t, b_valid_a, b_valid_r)
    else
      batch_deltas, batch_v = self:calc_deltas(b_valid_s, b_valid_t, b_valid_a, b_valid_r)
    end
    v = v + batch_v:sum()
    delta = delta + batch_deltas:abs():sum()
  end
  return v / valid_s:size(1), delta / valid_s:size(1)
end

function dqn:calc_targets_seq(batch_s, batch_t, batch_a, batch_r)

  local seq = batch_s:size(2)

  -- reset RNNs, prime target network(s) with the first step (as it will then evaluate from step 2)
  self.target_network:forget()
  self.target_network:forward(batch_s[{ {}, 1}])
  self.train_network:forget()
  if self.ddqn_network then
    self.ddqn_network:forget()
    self.ddqn_network:forward(batch_s[{ {}, 1}])
  end

  -- prepare caches for deltas and targets
  local targets = self:get_cache({ batch_s:size(1), seq-1, #self.actions }, '_targets_seq', true):fill(0)
  local deltas = self:get_cache({ batch_s:size(1), seq-1 }, '_deltas_seq', false):fill(0)
  local q2_max = self:get_cache({ batch_s:size(1), seq-1 }, '_q2_max_seq', false):fill(0)

  -- iterate over timesteps, perform forward passes
  for time = 1, seq-1 do -- 1st dim is batch, 2nd dimension is time

    -- if we're doing single step BP (karparthy), only generate targets for the last transition
    if self.single_step_bp and time < seq-1 then
      -- just forward pass the nets, generate the internal state
      self.train_network:forward(batch_s[{ {}, time }])
      self.target_network:forward(batch_s[{ {}, time+1 }])
      if self.ddqn_network then
        self.ddqn_network:forward(batch_s[{ {}, time+1 }])
      end
    else
      local time_deltas, time_q2_max, time_s, time_t, time_a, time_r
      if self.sarsa or self.qsarsa then
        -- sequence used by sarsa(lambda) is remainder of episode
        time_s = batch_s[{ {}, {time, seq} }]
        time_t = batch_t[{ {}, {time, seq} }]
        time_a = batch_a[{ {}, {time, seq} }]
        time_r = batch_r[{ {}, {time, seq}}]

        -- calc_sarsa_deltas will advance train_network one step forward by s_time
        if self.qsarsa then
          time_deltas, time_q2_max = self:calc_qsarsa_deltas(time_s, time_t, time_a, time_r)
        else
          time_deltas, time_q2_max = self:calc_sarsa_deltas(time_s, time_t, time_a, time_r)
        end
      else
        time_s = batch_s[{ {}, {time, time+1} }]
        time_t = batch_t[{ {}, {time, time+1} }]
        time_a = batch_a[{ {}, {time, time+1} }]
        time_r = batch_r[{ {}, {time, time+1}}]

        time_deltas, time_q2_max = self:calc_deltas(time_s, time_t, time_a, time_r)
      end

      -- copy targets and deltas
      deltas[{ {}, time}]:copy(time_deltas)
      q2_max[{ {}, time}]:copy(time_q2_max)

      for i = 1, targets:size(1) do -- iterate over the batches, copy for this timestep
        local a = time_a[i][1] -- first is a_t
        if a ~= 0 then
          targets[i][time][a] = deltas[i][time]
        end
      end
    end
  end

  return targets, deltas, q2_max
end

function dqn:calc_targets(batch_s, batch_t, batch_a, batch_r)
  -- extract the relevant action indices
  local a = batch_a[{{}, 1}] -- first is a_t, action taken at s_t

  -- calculate deltas
  local delta, q2_max = self:calc_deltas(batch_s, batch_t, batch_a, batch_r)

  -- Produce targets: sparse matrix of deltas for the action taken at that timestep: Q(s, a)
  local targets = self:get_cache({ batch_s:size(1), #self.actions }, '_targets', true):fill(0)
  for i = 1, targets:size(1) do
    if a[i] ~= 0 then
      targets[i][a[i]] = delta[i] -- copy delta into correct action index
    end
  end

  return targets, delta, q2_max
end

function dqn:calc_qsarsa_deltas(batch_s, batch_t, batch_a, batch_r)

  -- SARSA(LAMBDA) DELTAS
  -- delta = (SUM_t:1,T gamma^t * r_t) - Q(s_1, a_1)

  -- Compute 'Q(s_1, a_1)'
  local s = batch_s[{{}, 1}] -- first is state in questions
  local a = batch_a[{{}, 1}] -- first is action taken at state in question
  local q_all = self:cache_copy(self.train_network:forward(s), '_q_all', false) -- compute all q-values
  local q = self:get_cache({q_all:size(1)}, '_q', false):fill(0)
  local q_max = q_all:max(2)
  for i = 1, q_all:size(1) do
    if a[i] ~= 0 then
      q[i] = q_all[i][a[i]] -- copy the correct q-value for action taken at time 'i'
    end
  end

  -- Compute 'SUM_t:1,T gamma^t * r_t' (sum of discounted rewards in episode)
  local r = self:cache_copy(batch_r[{ {}, 1}], '_r', false)
  local sum_r = self:cache_copy(r, '_r_sum', false) -- initialise sum with r_1 in it
  local gamma = self.discount_gamma
  for t = 2, batch_r:size(2) do -- second dimension of batches is time
    r:copy(batch_r[{ {}, t}]):mul(gamma)
    sum_r:add(r)
    gamma = gamma * self.discount_gamma
  end

  -- Q-LEARNING DELTAS

  -- transform batch_t from 'term' to '1-term'
  local t = batch_t[{ {}, 2}]
  local term = self:cache_copy(t, '_term', false) -- copy to cache
  term = term:mul(-1):add(1)

  -- Compute all 'Q(s2, _)' values
  local s2 = batch_s[{ {}, 2}]
  local q2_all = self:cache_copy(self.target_network:forward(s2), '_q2_all', false)
  local q2 = self:get_cache({q2_all:size(1)}, '_q2', false):fill(0)
  if self.ddqn then
    -- take "Q'(s2, max_a Q(s2, a))" for Double Q-Learning
    -- if we're training on sequences then DDQN network will exist, otherwise we can just use the training network as it doesn't have internal state
    local ddqn_net = self.ddqn_network or self.train_network
    local q2_ddqn = ddqn_net:forward(s2)
    local _, max_a = q2_ddqn:max(2)
    q2:copy(q2_all:gather(2, max_a))
  else
    -- take "max_a Q'(s2, a)" for standard Q-learning
    q2 = q2_all:max(2)
  end
  -- Compute "(1-term) * gamma * [q2]"
  q2:mul(self.discount_gamma):cmul(term)

  -- Compute all deltas and clip

  sum_r:mul(1 - self.sigma)
  local delta = self:cache_copy(r, '_r', false)
  delta:add(q2):mul(self.sigma)

  delta:add(sum_r):add(-1, q)

  -- clip the deltas
  if self.clip_delta then
    delta[delta:ge(self.clip_delta)] = self.clip_delta
    delta[delta:le(-self.clip_delta)] = -self.clip_delta
  end

  return delta, q
end

function dqn:calc_sarsa_deltas(batch_s, batch_t, batch_a, batch_r)
  -- delta = (SUM_t:1,T gamma^t * r_t) - Q(s_1, a_1)

  -- Compute 'Q(s_1, a_1)'
  local s = batch_s[{{}, 1}] -- first is state in questions
  local a = batch_a[{{}, 1}] -- first is action taken at state in question
  local q_all = self:cache_copy(self.train_network:forward(s), '_q_all', false) -- compute all q-values
  local q = self:get_cache({q_all:size(1)}, '_q', false):fill(0)
  for i = 1, q_all:size(1) do
    if a[i] ~= 0 then
      q[i] = q_all[i][a[i]] -- copy the correct q-value for action taken at time 'i'
    end
  end

  -- Compute 'SUM_t:1,T gamma^t * r_t' (sum of discounted rewards in episode)
  local r = self:cache_copy(batch_r[{ {}, 1}], '_r', false)
  local sum_r = self:cache_copy(r, '_r_sum', false) -- initialise sum with r_1 in it
  local gamma = self.discount_gamma
  for t = 2, batch_r:size(2) do -- second dimension of batches is time
    r:copy(batch_r[{ {}, t}]):mul(gamma)
    sum_r:add(r)
    gamma = gamma * self.discount_gamma -- t
  end

  -- delta = (SUM_t:1,T gamma^t * r_t) - Q(s_1, a_1)
  local delta = sum_r:add(-1, q)

  -- clip the deltas
  if self.clip_delta then
    delta[delta:ge(self.clip_delta)] = self.clip_delta
    delta[delta:le(-self.clip_delta)] = -self.clip_delta
  end

  return delta, q
end

function dqn:calc_deltas(batch_s, batch_t, batch_a, batch_r)

  -- Need to compute for each timestep:
  -- delta = r + (1-term) * gamma * max_q Q(s2, a) - Q(s, a)

  local s = batch_s[{{}, 1}] -- sequence of 2, first is s_t
  local t = batch_t[{{}, 2}] -- second is whether s_t+1 is terminal
  local a = batch_a[{{}, 1}] -- first is a_t, action taken at s_t
  local r = batch_r[{{}, 2}] -- second is r_t+1, reward received on transition to s_t+1
  local s2 = batch_s[{{}, 2}] -- sequence of 2, second is s_t+1 (most recent first)
  local a2 = batch_a[{{}, 2}] -- sequence of 2, second is a_t+1 (most recent first)

  -- transform batch_t from 'term' to '1-term'
  local term = self:cache_copy(t, '_term', false) -- copy to cache
  term = term:mul(-1):add(1)

  -- Compute all 'Q(s2, _)' values
  local q2_all = self:cache_copy(self.target_network:forward(s2), '_q2_all', false)
  local q2 = self:get_cache({q2_all:size(1)}, '_q2', false):fill(0)
  if self.sarsa then
    -- take 'Q(s2, a2)' if SARSA learning enabled
    for i = 1, q2_all:size(1) do
      if a2[i] ~= 0 then
        q2[i] = q2_all[i][a2[i]] -- copy the correct q-value for action taken at time 'i'
      end
    end
  elseif self.ddqn then
    -- take "Q'(s2, max_a Q(s2, a))" for Double Q-Learning
    -- if we're training on sequences then DDQN network will exist, otherwise we can just use the training network as it doesn't have internal state
    local ddqn_net = self.ddqn_network or self.train_network
    local q2_ddqn = self:cache_copy(ddqn_net:forward(s2), '_ddqn_q2_all', false)
    local _, max_a = q2_ddqn:max(2)
    q2:copy(q2_all:gather(2, max_a))
  else
    -- take "max_a Q'(s2, a)" for standard Q-learning
    q2 = q2_all:max(2)
  end

  -- Compute "(1-term) * gamma * [q2]"
  q2:mul(self.discount_gamma):cmul(term)

  -- Compute "Q(s, a)" and "max_a Q(s, a)"
  local q_all = self:cache_copy(self.train_network:forward(s), '_q_all', false) -- compute all q-values
  local q = self:get_cache({q_all:size(1)}, '_q', false):fill(0)
  for i = 1, q_all:size(1) do
    if a[i] ~= 0 then
      q[i] = q_all[i][a[i]] -- copy the correct q-value for action taken at time 'i'
    end
  end
  local q_max = q_all:max(2)

  -- Compute all deltas and clip
  local delta = self:cache_copy(r, '_r', false)
  if self.advantage_learning then
    delta:add(q2):add(-1, q_max):div(self.advantage_kappa):add(q_max):add(-1, q) -- delta = max_a A (s, a) + (r + (1-term) * gamma * max_a A'(s2, a) - max_a A (s, a)) / k - A(s, a)
  else
    delta:add(q2):add(-1, q) -- delta = r + (1-term) * gamma * max_a Q'(s2, a) - Q(s, a)
  end

  -- clip the deltas
  if self.clip_delta then
    delta[delta:ge(self.clip_delta)] = self.clip_delta
    delta[delta:le(-self.clip_delta)] = -self.clip_delta
  end

  return delta, q2
end

function dqn.optim_eval(params)
  if params ~= _agent.w then
    _agent.w:copy(params)
  end

  -- reset gradients
  _agent.dw:zero()

  -- sequence training?
  local seq = 2
  if _agent.rho then
    seq = _agent.rho + 1 -- one ahead for s2
  end

  -- retrieve minibatch caches (with batch_s as the only GPU-cache)
  local batch_s = _agent:get_cache({_agent.batch_size, seq, _agent.tt.state_size + _agent.tt.extra_state_size}, '_batch_s', true)
  local batch_t = _agent:get_cache({_agent.batch_size, seq}, '_batch_t', false)
  local batch_a = _agent:get_cache({_agent.batch_size, seq}, '_batch_a', false)
  local batch_r = _agent:get_cache({_agent.batch_size, seq}, '_batch_r', false)
  local batch_max_len

  if _agent.sample_episodes then
    -- calculate episode length to sample at based on cached experience replay statistics
    if _agent.adaptive_seq then
      seq = math.min(math.ceil(_agent.tt.episode_median), seq)
      -- trim the batch if necessary
      batch_s = batch_s[{ {}, {1, seq}}]
      batch_t = batch_t[{ {}, {1, seq}}]
      batch_a = batch_a[{ {}, {1, seq}}]
      batch_r = batch_r[{ {}, {1, seq}}]
    end
    -- sample full episodes
    batch_max_len, batch_s, batch_t, batch_a, batch_r = _agent.tt:sample_episodes(_agent.batch_size, seq, batch_s, batch_t, batch_a, batch_r)
  else
    -- sample a minibatch of experiences
    batch_max_len, batch_s, batch_t, batch_a, batch_r = _agent.tt:sample(_agent.batch_size, seq, batch_s, batch_t, batch_a, batch_r)
  end

  -- rescale reward
  if _agent.rescale_r then
    _agent.r_max = math.max(batch_r:max(), _agent.r_max)
    batch_r:div(_agent.r_max)
  end

  local targets, deltas, q2_max
  if seq == 2 then
    targets, deltas, q2_max = _agent:calc_targets(batch_s, batch_t, batch_a, batch_r)
  else
    targets, deltas, q2_max = _agent:calc_targets_seq(batch_s, batch_t, batch_a, batch_r)
  end

  for time = seq, seq-batch_max_len+2, -1 do -- step backwards from end of tensor (end of sequence) to start of sequence
    local s = batch_s[{{}, time-1}] -- first is s_t, second is s_t+1
    local time_targets = targets
    if seq > 2 then time_targets = targets[{ {}, batch_max_len + (time-seq) - 1}] end -- targets given per timestep if training on a sequence
    -- populate model parameter gradients for this timestep
    _agent.train_network:backward(s, time_targets)

    -- break out of the loop if we are doing single step backprop
    if _agent.single_step_bp then
      break -- this is a pretty dirty way of contracting this loop...
    end
  end

  -- apply gradient clipping
  if _agent.clip_gradient then
        _agent.dw[_agent.dw:gt(_agent.clip_gradient)] = _agent.clip_gradient
        _agent.dw[_agent.dw:lt(-_agent.clip_gradient)] = -_agent.clip_gradient
  end

  -- multiply by -1 to reverse the gradients -- VERY IMPORTANT
  _agent.dw:mul(-1)

  return torch.abs(targets):sum(), _agent.dw
end

function dqn.preprocess_rgb(s)
  -- check the state dimensions are as expected
  assert(s:dim() == 3 or s:dim() == 4)
  -- convert RGB to Y
  local s = s
  if s:dim() == 3 then
    -- convert single RGB frame
    return image.rgb2y(s)
  else
    -- check the buffer is ready
    _preprocess_buf = _preprocess_buf or torch.FloatTensor()
    if _preprocess_buf:nElement() ~= (s:size(1) * s:size(3) * s:size(4)) then
      _preprocess_buf:resize(s:size(1), s:size(3), s:size(4))
    end
    -- convert frames and store in buffer
    for i = 1, s:size(1) do
      _preprocess_buf[i]:copy(image.rgb2y(s[i]))
    end
    return _preprocess_buf
  end
end

return dqn
