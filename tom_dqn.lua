-- require stuff
require 'nn'
require 'cunn'
require 'image'

require 'utils'

Wrapper = require 'WrapperDQN'
TomDQN = require 'TomDQN'

-- save the options so we can check them out later
torch.save(job_dir .. "opt.t7", opt)

-- log whether or not GPU-accelerated
local flag = "disabled"
if opt.gpu then
  cutorch.setDevice(opt.gpu)
  flag = "enabled (GPU" .. opt.gpu .. ")"
end
log(string.format('GPU acceleration is %s', flag))

-- set the CPU threads
if opt.cpu_threads then
  torch.setnumthreads(opt.cpu_threads)
end
log(string.format('Running on %d CPU threads.', torch.getnumthreads()))

---------------------------------
-- CREATE THE GAME ENVIRONMENT --
---------------------------------
opt.listeners = opt.listeners or {}

if opt.provide_objective then
    local ProvideObjectiveListener = require('ProvideObjectiveListener')
    table.insert(opt.listeners, ProvideObjectiveListener:new())
end

if (opt.memory_size and opt.memory_size > 0) then
  local ReinforcementMemoryListener = require('ReinforcementMemoryListener')
  table.insert(opt.listeners, ReinforcementMemoryListener:new({ memory_size = opt.memory_size, screen_sizes = torch.LongStorage(opt.input_dims) }))
end

if opt.ws_server then
    local WebSocketGameListener = require('WebSocketGameListener')
    local listener = WebSocketGameListener:new({ host = opt.ws_server, name = opt.game_name, timeout = 0.05 })
    function listener:encodeState(game)
      -- return a single element array with the game screen
      local canvas = { self.encodeScreen(game.render.canvas.pixels) }
      -- if _state is set, encode those screens instead
      if game._state then
        canvas = {}
        for i = 1, game._state:size(1) do
          table.insert(canvas, self.encodeScreen(game._state[i]))
        end
      end
      return canvas
    end
    table.insert(opt.listeners, listener)
end

if opt.game_class == 'HonoursGame' or opt.game_class == 'HonoursGameSmall' or opt.game_class == 'HonoursGameXS' then
    opt.atlas_file = opt.atlas_file or '/root/notebook/luagamesimulations/honours/atlas2.png'
    opt.font_file = opt.font_file or '/root/notebook/luagamesimulations/pixel.font'
    local function reset_func(game, opt)
        game.numGoalObjects = math.min(3, opt.goals) or 1
        game.numScreenObjects = math.min(4, opt.goals) or 1
        game.stepScore = 0
        game.goalScore = opt.goal_score or 4
        game.badGoalScore = opt.bad_goal_score or -0.1
        game.maxNumGoals = opt.max_goals or 4
        game.numGoalTypes = opt.goal_types or game.maxNumGoals
    end
    opt.reset_func = reset_func
end


log(string.format('Creating instance of \'%s\' named \'%s\'', opt.game_class, opt.game_name))
opt.game_class = require(opt.game_class)
if opt.game_nowrap then
  game_env = opt.game_class:init(opt)
else
  game_env = Wrapper:init(opt)
end
opt.actions = game_env:getActions()
opt.game_class = nil

---------------------------------
--      CREATE THE AGENT       --
---------------------------------

-- change some parameters of input_dims if we are enabling memory or using provide objective
if opt.provide_objective then
  opt.input_dims[1] = 2
elseif opt.memory_size and opt.memory_size > 0 then
  opt.input_dims[1] = opt.memory_size + 1
end

-- create the model
local model_func = require(opt.model_loader)
local model = model_func(opt)
model:float()

-- calculate flattened state size
local state_size = opt.input_dims[1]
for i = 2, #opt.input_dims do
  state_size = state_size * opt.input_dims[i]
end

-- define the agent parameters
agent_opt = {
  preproc_func = opt.preproc_func or TomDQN.preprocess_rgb,
  model = model,
  actions = opt.actions,
  -- q-learning parameters
  discount_gamma = opt.discount_gamma or 0.99,
  adaptive_seq = opt.adaptive_seq,
  sample_episodes = opt.sample_episodes,
  sarsa = opt.sarsa, -- use SARSA-learning rather than Q-learning?
  qsarsa = opt.qsarsa, -- use Q-SARSA learning?
  sigma_start_steps = opt.sigma_start_steps,
  sigma_steps = opt.sigma_steps,
  sigma_start = opt.sigma_start,
  sigma_end = opt.sigma_end,
  rho = opt.rho, -- sequence length for BPTT
  ddqn = opt.ddqn, -- use Double DQN? van Hasselt (2015)
  single_step_bp = opt.single_step_bp, -- only perform single backward pass on RNN training? (Kaparthy tweet?)
  advantage_learning = opt.advantage_learning, -- utilise Advantage learning instead of Q-learning?
  advantage_kappa = opt.advantage_kappa, -- kappa constant for Advantage learning
  -- training parameters
  train_freq = opt.train_freq or 4, -- number of steps between training updates
  train_batches = opt.train_batches or 1, -- number of minibatches to train each update
  batch_size = opt.batch_size or 128, -- size of training minibatch
  target_q_freq = opt.target_q_freq or 10000, -- steps between snapshotting model as target
  clip_delta = opt.clip_delta or 1, -- clamp bounds for Q-learning targets
  clip_gradient = opt.clip_gradient, -- clamp bounds for gradients
  optim_method = opt.optim_method,
  optim_state = opt.optim_state,
  -- reward parameters for reward processing
  min_reward = opt.min_reward or -1,
  max_reward = opt.max_reward or 1,
  rescale_r = opt.rescale_r or 1,
  -- epsilon parameters for e-greedy policy
  ep_start = opt.ep_start or 1.0, -- initial value of ep
  ep_end = opt.ep_end or 0.1, -- final value of ep
  ep_steps = opt.ep_steps or 1000000, -- num steps to linearly anneal ep over
  ep_test = opt.ep_test or 0.05, -- ep to use during evaluation
  -- transition table
  state_size = state_size, -- size of flattened state
  size = opt.tt_size or 1000000, -- maximum capacity of the transition table before overwrite
  non_reward_prob = 0, -- probability of discarding a sampled sequence that contains no reward signal
  -- gpu acceleration
  gpu = opt.gpu
}

log(string.format('Using \'%s\' optimisation (default is deepmind_rmsprop)', (opt.optim_method or 'undefined')))
agent = TomDQN:new(agent_opt)
_agent = agent -- populate agent into a known global variable for use by optim

---------------------------------
--      TRAIN THE AGENT        --
---------------------------------

n_episodes = {} -- Episodes completed during evaluation
n_rewards_pos = {} -- Number of positive reward states encountered during evaluation
n_rewards_neg = {} -- Number of negative reward states encountered during evaluation
time_history = {} -- Time taken to complete each train-eval epoch
reward_history = {} -- Sum of reward collected during each evaluation
td_history = {} -- Average temporal difference error over the validation set
v_history = {} -- Average V for all states over the validation set

tt_n_episodes = {} -- number of full episodes in experience replay
tt_med_length = {} -- median length of episodes in experience replay
tt_avg_length = {} -- average length of episodes in experience replay
tt_std_length = {} -- standard deviation of episode lengths in experience replay
tt_min_length = {} -- shortest length episode in experience replay
tt_max_length = {} -- longest length episode in experience replay
tt_n_samples = {} -- number of samples taken from transition table this epoch
tt_n_discards = {} -- number of discarded samples from transition table this epoch

-- run a bunch of random steps before training to populate the transition table
local start_step = opt.start_step or 1
local run_time = os.time()
local pre_episodes, pre_rewards_pos, pre_rewards_neg, pre_reward = agent:run(start_step, opt.pretrain_steps, game_env, nil, false, true, true)
run_time = os.time() - run_time
log('Pre-train steps complete.')
log(string.format('n_episodes=%d\tn_rewards_pos=%d\tn_rewards_neg=%d\treward_history=%.1f', pre_episodes, pre_rewards_pos, pre_rewards_neg, pre_reward))

-- sample a validation set used to gather agent stats during training
local valid_max_len, valid_s, valid_t, valid_a, valid_r
if opt.rho then
  local valid_seq_len = opt.rho + 1
  if opt.sample_episodes then
    valid_max_len, valid_s, valid_t, valid_a, valid_r = agent.tt:sample_episodes(opt.valid_size, valid_seq_len)
  else
    valid_max_len, valid_s, valid_t, valid_a, valid_r = agent.tt:sample(opt.valid_size, valid_seq_len)
  end
else
  valid_max_len, valid_s, valid_t, valid_a, valid_r = agent.tt:sample(opt.valid_size, 2) -- sequence length of 2 for single transitions
end

if opt.gpu then
  valid_s = valid_s:cuda()
end
log('Sampled validation set of size ' .. opt.valid_size)

-- initialise the statistics in the experience replay
log(string.format('tt_n_episodes=%d\ttt_med_length=%.1f\ttt_avg_length=%.1f\ttt_std_length=%.1f\ttt_min_length=%.1f\ttt_max_length=%.1f\ttt_n_samples=%.1f\ttt_n_discards=%.1f', agent.tt:statistics()))

-- train for a set of train-eval epochs
local train_time, eval_time
for e = 1, opt.epochs do

  -- train the agent
  log(string.format('Training %d steps for epoch %d...', opt.train_steps, e))
  train_time = os.time()
  local tr_episodes, tr_rewards_pos, tr_rewards_neg, tr_reward = agent:train(start_step + (e-1) * opt.train_steps, opt.train_steps, game_env)
  log(string.format('n_episodes=%d\tn_rewards_pos=%d\tn_rewards_neg=%d\treward_history=%.1f', tr_episodes, tr_rewards_pos, tr_rewards_neg, tr_reward))
  train_time = os.time() - train_time

  -- evaluate the agent on the validation set
  log(string.format('Evaluating validation set epoch %d...', e))
  v_history[e], td_history[e] = agent:calc_validation(valid_s, valid_t, valid_a, valid_r)
  log(string.format('v_history=%.4f\ttd_history=%.4f', v_history[e], td_history[e]))

  -- analyse the experience replay memory
  tt_n_episodes[e], tt_med_length[e], tt_avg_length[e], tt_std_length[e], tt_min_length[e], tt_max_length[e], tt_n_samples[e], tt_n_discards[e] = agent.tt:statistics()
  log(string.format('tt_n_episodes=%d\ttt_med_length=%.1f\ttt_avg_length=%.1f\ttt_std_length=%.1f\ttt_min_length=%.1f\ttt_max_length=%.1f\ttt_n_samples=%.1f\ttt_n_discards=%.1f', tt_n_episodes[e], tt_med_length[e], tt_avg_length[e], tt_std_length[e], tt_min_length[e], tt_max_length[e], tt_n_samples[e], tt_n_discards[e]))

  -- evaluate the agent's policy
  log(string.format('Evaluating %d steps for epoch %d...', opt.eval_steps, e))
  eval_time = os.time()
  n_episodes[e], n_rewards_pos[e], n_rewards_neg[e], reward_history[e] = agent:evaluate(opt.eval_steps, game_env)
  eval_time = os.time() - eval_time
  log(string.format('n_episodes=%d\tn_rewards_pos=%d\tn_rewards_neg=%d\treward_history=%.1f', n_episodes[e], n_rewards_pos[e], n_rewards_neg[e], reward_history[e]))
  -- record time taken
  time_history[e] = train_time + eval_time

  -- save the network if it is the best performing so far
  if #reward_history == 1 or reward_history[e] > torch.Tensor(reward_history):max() then
    log('Saving best agent at epoch ' .. e .. '...')
    agent.best_network = agent.eval_network:clone()
    agent.best_network:clearState()
  end

  -- save
  if e % opt.save_freq == 0 or e == opt.epochs then
      local filename = opt.save_name
      local listeners = opt.listeners
      opt.listeners = nil -- writing WebSocketGameListener craps out for some reason, so just avoid saving the listeners array
      torch.save(job_dir .. filename .. ".t7", {
                              reward_history = reward_history,
                              n_rewards_pos = n_rewards_pos,
                              n_rewards_neg = n_rewards_neg,
                              n_episodes = n_episodes,
                              time_history = time_history,
                              v_history = v_history,
                              td_history = td_history,
                              tt_n_episodes = tt_n_episodes,
                              tt_med_length = tt_med_length,
                              tt_avg_length = tt_avg_length,
                              tt_std_length = tt_std_length,
                              tt_min_length = tt_min_length,
                              tt_max_length = tt_max_length,
                              tt_n_samples = tt_n_samples,
                              tt_n_discards = tt_n_discards,
                              opt = opt})
      opt.listeners = listeners -- restore the listeners array
      -- cleanup networks
      if opt.rho then
        agent.train_network:forget()
      end
      local cache_network = cleanupModel(agent.train_network)
      torch.save(job_dir .. filename .. "_net.t7", agent.train_network)
      restoreModel(agent.train_network, cache_network)
      if agent.best_network then
        if opt.rho then
          agent.best_network:forget()
        end
        torch.save(job_dir .. filename .. "_best_net.t7", agent.best_network)
      end
      log('Saved: ' .. filename .. '.t7')
      io.flush()
      collectgarbage()
  end
end
print('Complete!')
