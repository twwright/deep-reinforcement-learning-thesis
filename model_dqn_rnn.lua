require 'nn'
require 'cunn'
require 'rnn'

function create_model(opt)
  local net = nn.Sequential()
  -- ensure our input is not flattened
  net:add(nn.Reshape(unpack(opt.input_dims)))

  -- model as defined by DQN, or use a custom conv net
  local layers = opt.model_layers or {{32, 8, 4, 0}, {64, 4, 2, 0}, {64, 3, 1, 0}}

  -- convolutional layers
  for i = 1, #layers do
    local in_maps = opt.input_dims[1]
    if i > 1 then in_maps = layers[i-1][1] end
    net:add(nn.SpatialConvolution(
      in_maps, layers[i][1],
      layers[i][2], layers[i][2],
      layers[i][3], layers[i][3],
      layers[i][4], layers[i][4]))
    net:add(nn.ReLU())
  end

  -- forward pass to determine output size so far
  local n_conv_out = net:forward(torch.zeros(unpack(opt.input_dims))):nElement()
  -- flatten for input to linear layers
  net:add(nn.Reshape(n_conv_out))

  -- Recurrent layer
  if opt.is_gru then
    net:add(nn.GRU(n_conv_out, opt.rnn_dim, nil, 0.25))
  else
    net:add(nn.FastLSTM(n_conv_out, opt.rnn_dim))
  end
  --net:add(nn.ReLU()) -- DRQN paper recommends no ReLU here

  -- final fully-connected layer to actions
  net:add(nn.Linear(opt.rnn_dim, #opt.actions))

  -- wrap in a recursor
  return nn.Recursor(net)
end

return create_model
