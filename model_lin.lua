require 'nn'
require 'cunn'

function create_model(opt)
  local net = nn.Sequential()

  -- calculate flattened state size
  local state_size = opt.input_dims[1]
  for i = 2, #opt.input_dims do
    state_size = state_size * opt.input_dims[i]
  end

  local prev_dim = state_size
  -- add hidden layers
  for i = 1, #opt.hidden_dim do
    net:add(nn.Linear(prev_dim, opt.hidden_dim[i]))
    net:add(nn.ReLU())
    prev_dim = opt.hidden_dim[i]
  end

  -- final fully-connected layer to actions
  net:add(nn.Linear(prev_dim, #opt.actions))

  -- wrap in a recursor
  return net
end

return create_model
