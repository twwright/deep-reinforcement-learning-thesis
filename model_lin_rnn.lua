require 'nn'
require 'cunn'
require 'rnn'

function create_model(opt)
  local net = nn.Sequential()

  -- calculate flattened state size
  local state_size = opt.input_dims[1]
  for i = 2, #opt.input_dims do
    state_size = state_size * opt.input_dims[i]
  end

  local prev_dim = state_size

  -- add hidden layers
  for i = 1, #opt.hidden_dim do
    net:add(nn.Linear(prev_dim, opt.hidden_dim[i]))
    net:add(nn.ReLU())
    prev_dim = opt.hidden_dim[i]
  end

  -- Recurrent layer
  if opt.is_gru then
    net:add(nn.GRU(prev_dim, opt.rnn_dim, nil, 0.25))
  else
    net:add(nn.FastLSTM(prev_dim, opt.rnn_dim))
  end
  --net:add(nn.ReLU())

  -- final fully-connected layer to actions
  net:add(nn.Linear(opt.rnn_dim, #opt.actions))

  -- wrap in a recursor
  return nn.Recursor(net)
end

return create_model
